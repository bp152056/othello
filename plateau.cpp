#include <iostream>
#include "plateau.h"
#include "console.h"
#include <vector>
#include <fstream>


//constructeur du plateau
Plateau::Plateau()
{

    for(int i=0; i<8; i++)
    {
        for(int j=0; j<8; j++)
        {
            //creation des cases du plateau
            tab[i][j]= 0;
        }
    }

    //poser 4 pions d�but partie
    tab[3][3]=1;
    tab[4][3]=2;
    tab[3][4]=2;
    tab[4][4]=1;


}

Plateau::~Plateau()
{

}
//retourne une case du plateau par rapport a ses coordonn�es
int Plateau::gettab(int _x, int _y)
{
    return(tab[_x-1][_y-1]);
}

void Plateau::settab(int _x, int _y, int _new)
{
    tab[_x-1][_y-1]=_new;
}

void Plateau::afficherPions()
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    for (int i=0; i<8; i++)
    {
        for (int j=0; j<8; j++)
        {
            if (tab[i][j]!=0)
            {
                pConsole->gotoLigCol(2*(i+1),1+ 4*(j+1));
                if(tab[i][j]== 1)
                {
                    std::cout << " ";
                }
                else std::cout << char(219);
            }
        }
    }

    Console::deleteInstance();
}
void Plateau::afficherPlateau()
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    pConsole->gotoLigCol(0,0);

    std::ifstream fichier("plateau.txt",std::ios::in);
    if(fichier)
    {
        std ::string ligne;
        while(!fichier.eof())
        {
            getline(fichier,ligne);
            for (unsigned int i=0; i<ligne.length(); i++)
            {
                if (ligne[i] == 'O')
                {
                    pConsole->setColor(COLOR_BROWN);
                    std::cout << char(219);
                    pConsole->setColor(COLOR_DEFAULT);
                }

                if (ligne[i] == 'a')
                {
                    pConsole->setColor(COLOR_GREEN);
                    std::cout << ' ';
                    pConsole->setColor(COLOR_DEFAULT);
                }

                if (ligne[i] != 'O' && ligne[i] != 'Z' && ligne[i] != 'a')
                    std::cout << ligne[i];


                if (ligne[i] == 'Z')
                    std::cout << std::endl;
            }
        }
    }
    else
        std::cerr << "soucis ouverture fichier" << std::endl;

    afficherPions();

    Console::deleteInstance();
}

