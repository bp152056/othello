#ifndef PLATEAU_H_INCLUDED
#define PLATEAU_H_INCLUDED

#include <iostream>
#include <vector>



//implementation du plateau (1 seul pour une partie)
class Plateau
{
private:
    //declaration du tableau de cases du tableau
    int tab[8][8];

public:
    Plateau();
    ~Plateau();
    //retourne la case du tableau demand�
    int gettab(int _x,int _y);
    void settab(int _x,int _y,int _new);

    void afficherPlateau ();
    void afficherPions();


};

#endif // PLATEAU_H_INCLUDED

