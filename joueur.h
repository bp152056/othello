#ifndef JOUEUR_H_INCLUDED
#define JOUEUR_H_INCLUDED

#include <iostream>
#include <vector>
#include "console.h"
#include "plateau.h"

//implementation d'un joueur
class Joueur
{
private:
    bool m_cqui;// J1=0 J2=1
    int m_tour;
    bool m_ia;
    bool tabia[8][8];


public:
    Joueur(bool _cqui,bool _ia);
    ~Joueur();

    bool getcqui();
    bool getia();
    int lautre();

    bool cestpossibleoupas(Plateau *p);
    void poserPion(Plateau *p, int tour);
    void fichaJ();
    bool verifretourner(Plateau *p,int x,int y);
    void retourner(Plateau *p,int x,int y,int comptx, int compty, int sensx,int sensy);

     void remplirtabia(Plateau *p);
     void vidertabia(Plateau *p);
     void affichtabia();
     void poseria(Plateau *p);

};

#endif // JOUEUR_H_INCLUDED

