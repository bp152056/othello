#ifndef PARTIE_H_INCLUDED
#define PARTIE_H_INCLUDED
#include "joueur.h"
#include "ia1.h"
#include "plateau.h"
#include "console.h"
#include<vector>
#include<iostream>

class Partie
{
private:
    bool caqui;
    int m_nbtour;
    std::vector <Joueur> j;
    Plateau p;


public:
    Partie();
    ~Partie();
    bool getCaqui();
    void setCaqui();
    Joueur getJoueur(int num);
    Plateau getPlateau();

    void Menu();
    int choixMenu();
    void commencerPartie();
    void kikagagne();

    int getTour();
    void tourPlus ();



};

#endif // PARTIE_H_INCLUDED

