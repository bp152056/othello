#include <iostream>
#include "joueur.h"
#include "console.h"
#include <cstdlib>
#include <ctime>


using namespace std;

Joueur::Joueur(bool _cqui,bool _ia)
    :m_cqui(_cqui),m_ia(_ia)
{
     for (int i=0; i<8; i++)
    {
        for (int j=0; j<8; j++)
        {
            tabia[i][j]=0;
        }
    }

}

Joueur::~Joueur()
{

}

bool Joueur::getcqui()
{
    return (m_cqui);
}
bool Joueur::getia()
{
    return(m_ia);
}
void Joueur::fichaJ()
{
    std::cout << "J" << getcqui()+1 << " JOUE";
}
int Joueur::lautre()
{
    if(m_cqui==0)
    {
        return 2;
    }
    else
    {
        return 1;
    }
}
bool Joueur::cestpossibleoupas(Plateau *p)
{
    int re=0;
     int comptx;
    int compty;

    for (int i=1; i<9; i++)
    {
        for (int j=1; j<9; j++)
        {
            if (p->gettab(j,i)==0)
            {
                // poser � gauche
                comptx=i;
                compty=j;

                do
                {
                    comptx++;

                }
                while(p->gettab(j,comptx)==lautre() && comptx<9);
                if(comptx!=9 && p->gettab(j,comptx)==getcqui()+1 && p->gettab(j,i+1)==lautre())
                {
                    re=1;
                }

                //poser � droite
                comptx=i;
                compty=j;
                do
                {
                    comptx--;

                }
                while(p->gettab(j,comptx)==lautre() && comptx>0);
                if(comptx!=0 && p->gettab(j,comptx)==getcqui()+1&& p->gettab(j,i-1)==lautre())
                {
                   re=1;
                }

                //poser en haut
                comptx=i;
                compty=j;
                do
                {
                    compty++;

                }
                while(p->gettab(compty,i)==lautre() && compty<9);
                if(compty!=9 && p->gettab(compty,i)==getcqui()+1&& p->gettab(i+1,j)==lautre())
                {
                    re=1;
                }

                //poser en bas
                comptx=i;
                compty=j;
                do
                {
                    compty--;

                }
                while(p->gettab(compty,i)==lautre() && compty>0);
                if(compty!=0 && p->gettab(compty,i)==getcqui()+1&& p->gettab(j-1,i)==lautre())
                {
                   re=1;
                }

                // poser en haut � gauche
                comptx=i;
                compty=j;

                do
                {
                    compty++;
                    comptx++;

                }
                while(p->gettab(compty,comptx)==lautre() && comptx<9 && compty<9);
                if(comptx!=9 && compty!=9 && p->gettab(compty,comptx)==getcqui()+1&& p->gettab(j+1,i+1)==lautre())
                {
                   re=1;
                }

                // poser en haut � droite
                comptx=i;
                compty=j;

                do
                {
                    compty++;
                    comptx--;

                }
                while(p->gettab(compty,comptx)==lautre() && comptx>0 && compty<9);
                if(comptx!=0 && compty!=9 && p->gettab(compty,comptx)==getcqui()+1&& p->gettab(j+1,i-1)==lautre())
                {
                    re=1;
                }

                // poser en bas � gauche
                comptx=i;
                compty=j;

                do
                {
                    compty--;
                    comptx++;

                }
                while(p->gettab(compty,comptx)==lautre() && comptx<9 && compty>0);
                if(comptx!=9 && compty!=0 && p->gettab(compty,comptx)==getcqui()+1&& p->gettab(j-1,i+1)==lautre())
                {
                   re=1;
                }


                // poser en bas � droite
                comptx=i;
                compty=j;

                do
                {
                    compty--;
                    comptx--;

                }
                while(p->gettab(compty,comptx)==lautre() && comptx>0 && compty>0);
                if(comptx!=0 && compty!=0 && p->gettab(compty,comptx)==getcqui()+1 && p->gettab(j-1,i-1)==lautre())
                {
                    re=1;
                }

            }
        }
    }
    if(re==0)
    {

    }
return re;
}
bool Joueur::verifretourner(Plateau *p,int x,int y)
{
    bool re=0;
    int comptx;
    int compty;
    if (p->gettab(y,x)==0)
    {
        // poser � gauche
        comptx=x;
        compty=y;

        do
        {
            comptx++;

        }
        while(p->gettab(y,comptx)==lautre());
        if( p->gettab(y,comptx)==m_cqui+1 && p->gettab(y,x+1)==lautre())
        {
            re=1;
            retourner(p,x,y,comptx,compty,-1,0);
        }

        //poser � droite
        comptx=x;
        compty=y;
        do
        {
            comptx--;

        }
        while(p->gettab(y,comptx)==lautre() && comptx>0);
        if(comptx!=0 && p->gettab(y,comptx)==m_cqui+1&& p->gettab(y,x-1)==lautre())
        {
            re=1;
            retourner(p,x,y,comptx,compty,1,0);
        }

        //poser en haut
        comptx=x;
        compty=y;
        do
        {
            compty++;

        }
        while(p->gettab(compty,x)==lautre() && compty<9);
        if(compty!=9 && p->gettab(compty,x)==m_cqui+1&& p->gettab(y+1,x)==lautre())
        {
            re=1;
            retourner(p,x,y,comptx,compty,0,-1);
        }

        //poser en bas
        comptx=x;
        compty=y;
        do
        {
            compty--;

        }
        while(p->gettab(compty,x)==lautre() && compty>0);
        if(compty!=0 && p->gettab(compty,x)==m_cqui+1&& p->gettab(y-1,x)==lautre())
        {
            re=1;
            retourner(p,x,y,comptx,compty,0,1);
        }

        // poser en haut � gauche
        comptx=x;
        compty=y;

        do
        {
            compty++;
            comptx++;

        }
        while(p->gettab(compty,comptx)==lautre() && comptx<9 && compty<9);
        if(comptx!=9 && compty!=9 && p->gettab(compty,comptx)==m_cqui+1&& p->gettab(y+1,x+1)==lautre())
        {
            re=1;
            retourner(p,x,y,comptx,compty,-1,-1);
        }

        // poser en haut � droite
        comptx=x;
        compty=y;

        do
        {
            compty++;
            comptx--;

        }
        while(p->gettab(compty,comptx)==lautre() && comptx>0 && compty<9);
        if(comptx!=0 && compty!=9 && p->gettab(compty,comptx)==m_cqui+1&& p->gettab(y+1,x-1)==lautre())
        {
            re=1;
            retourner(p,x,y,comptx,compty,1,-1);
        }

        // poser en bas � gauche
        comptx=x;
        compty=y;

        do
        {
            compty--;
            comptx++;

        }
        while(p->gettab(compty,comptx)==lautre() && comptx<9 && compty>0);
        if(comptx!=9 && compty!=0 && p->gettab(compty,comptx)==m_cqui+1&& p->gettab(y-1,x+1)==lautre())
        {
            re=1;
            retourner(p,x,y,comptx,compty,-1,1);
        }


        // poser en bas � droite
        comptx=x;
        compty=y;

        do
        {
            compty--;
            comptx--;

        }
        while(p->gettab(compty,comptx)==lautre() && comptx>0 && compty>0);
        if(comptx!=0 && compty!=0 && p->gettab(compty,comptx)==m_cqui+1 && p->gettab(y-1,x-1)==lautre())
        {
            re=1;
            retourner(p,x,y,comptx,compty,1,1);
        }

    }
    return re;
}

void Joueur::retourner(Plateau *p,int x,int y,int comptx, int compty, int sensx,int sensy)
{

    do
    {
        comptx=comptx+sensx;
        compty=compty+sensy;
        p->settab(compty,comptx,m_cqui+1);

    }
    while(comptx!=x || compty!=y);
}
void Joueur::poserPion(Plateau *p, int tour)
{
    bool stop=0;
    int x;
    int y;

    Console* pConsole = NULL;
    pConsole = Console::getInstance();

    pConsole->gotoLigCol(0,42);
    fichaJ();
std:
    cout << "   Tour :" << tour;

    pConsole->gotoLigCol(2,5);


    while(stop!=1)
    {
        if(pConsole->isKeyboardPressed())
        {
            int key = pConsole->getInputKey();

            if (key == 72) //d�placement curseur haut
            {

                pConsole->moveCurseur(-2,0);
                if(pConsole->getLig()==0)
                {
                    pConsole->gotoLigCol(16,pConsole->getCol());
                }

            }

            if (key == 80 ) //d�placement curseur bas
            {

                pConsole->moveCurseur(2,0);
                if(pConsole->getLig()==18)
                {
                    pConsole->gotoLigCol(2,pConsole->getCol());
                }


            }

            if (key == 77) //d�placement curseur droite
            {

                pConsole->moveCurseur(0,4);
                if(pConsole->getCol()==37)
                {
                    pConsole->gotoLigCol(pConsole->getLig(),5);
                }



            }

            if (key == 75 ) //d�placement curseur gauche
            {

                pConsole->moveCurseur(0,-4);
                if(pConsole->getCol()==1)
                {
                    pConsole->gotoLigCol(pConsole->getLig(),33);
                }

            }
            pConsole->gotoLigCol(pConsole->getLig(),pConsole->getCol());

            if (key == 13)
            {
                x=((pConsole->getCol())-1)/4;
                y=(pConsole->getLig())/2;
                if(verifretourner(p,x,y))
                {
                    p->settab(y,x,m_cqui+1);
                    stop=1;
                }
            }
        }
    }

    Console::deleteInstance(); // Lib�re la m�moire du pointeur !

}
void Joueur::vidertabia(Plateau *p)
{
    for (int i=0; i<8; i++)
    {
        for (int j=0; j<8; j++)
        {
            tabia[i][j]=0;

        }
    }

}
void Joueur::remplirtabia(Plateau *p)
{
    int comptx;
    int compty;

    for (int i=1; i<9; i++)
    {
        for (int j=1; j<9; j++)
        {
            if (p->gettab(j,i)==0)
            {
                // poser � gauche
                comptx=i;
                compty=j;

                do
                {
                    comptx++;

                }
                while(p->gettab(j,comptx)==lautre() && comptx<9);
                if(comptx!=9 && p->gettab(j,comptx)==getcqui()+1 && p->gettab(j,i+1)==lautre())
                {
                    tabia[i-1][j-1]=1;
                }

                //poser � droite
                comptx=i;
                compty=j;
                do
                {
                    comptx--;

                }
                while(p->gettab(j,comptx)==lautre() && comptx>0);
                if(comptx!=0 && p->gettab(j,comptx)==getcqui()+1&& p->gettab(j,i-1)==lautre())
                {
                    tabia[i-1][j-1]=1;
                }

                //poser en haut
                comptx=i;
                compty=j;
                do
                {
                    compty++;

                }
                while(p->gettab(compty,i)==lautre() && compty<9);
                if(compty!=9 && p->gettab(compty,i)==getcqui()+1&& p->gettab(i+1,j)==lautre())
                {
                    tabia[i-1][j-1]=1;
                }

                //poser en bas
                comptx=i;
                compty=j;
                do
                {
                    compty--;

                }
                while(p->gettab(compty,i)==lautre() && compty>0);
                if(compty!=0 && p->gettab(compty,i)==getcqui()+1&& p->gettab(j-1,i)==lautre())
                {
                    tabia[i-1][j-1]=1;
                }

                // poser en haut � gauche
                comptx=i;
                compty=j;

                do
                {
                    compty++;
                    comptx++;

                }
                while(p->gettab(compty,comptx)==lautre() && comptx<9 && compty<9);
                if(comptx!=9 && compty!=9 && p->gettab(compty,comptx)==getcqui()+1&& p->gettab(j+1,i+1)==lautre())
                {
                   tabia[i-1][j-1]=1;
                }

                // poser en haut � droite
                comptx=i;
                compty=j;

                do
                {
                    compty++;
                    comptx--;

                }
                while(p->gettab(compty,comptx)==lautre() && comptx>0 && compty<9);
                if(comptx!=0 && compty!=9 && p->gettab(compty,comptx)==getcqui()+1&& p->gettab(j+1,i-1)==lautre())
                {
                    tabia[i-1][j-1]=1;
                }

                // poser en bas � gauche
                comptx=i;
                compty=j;

                do
                {
                    compty--;
                    comptx++;

                }
                while(p->gettab(compty,comptx)==lautre() && comptx<9 && compty>0);
                if(comptx!=9 && compty!=0 && p->gettab(compty,comptx)==getcqui()+1&& p->gettab(j-1,i+1)==lautre())
                {
                    tabia[i-1][j-1]=1;
                }


                // poser en bas � droite
                comptx=i;
                compty=j;

                do
                {
                    compty--;
                    comptx--;

                }
                while(p->gettab(compty,comptx)==lautre() && comptx>0 && compty>0);
                if(comptx!=0 && compty!=0 && p->gettab(compty,comptx)==getcqui()+1 && p->gettab(j-1,i-1)==lautre())
                {
                    tabia[i-1][j-1]=1;
                }

            }
        }
    }

}

void Joueur::affichtabia()
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    pConsole->gotoLigCol(20,0);
    for (int i=0; i<8; i++)
    {
        for (int j=0; j<8; j++)
        {
            if(tabia[i][j]==1)
            {
                std::cout<<i+1<<" "<<j+1<<std::endl;
            }
        }
    }

}

void Joueur::poseria(Plateau *p)
{
    int compt=0;
    int nb=0;
     Console* pConsole = NULL;
    pConsole = Console::getInstance();

    std::vector<int> tabx;
    std::vector<int> taby;
    srand(time(NULL));
    remplirtabia(p);
     for (int i=0; i<8; i++)
    {
        for (int j=0; j<8; j++)
        {
            if(tabia[i][j]==1)
            {
               tabx.push_back(i);
               taby.push_back(j);
               compt++;
            }
        }
    }
    nb=rand()%compt;
     if(verifretourner(p,tabx[nb]+1,taby[nb]+1))
                {
                    p->settab(taby[nb]+1,tabx[nb]+1,m_cqui+1);
                }
    /*verifretourner(p,tabx[nb],taby[nb]);
    p->settab(taby[nb],tabx[nb],m_cqui+1);*/

    pConsole->gotoLigCol(18,0);
    std::cout<<tabx[nb]+1<<" "<<taby[nb]+1<<std::endl;


}


