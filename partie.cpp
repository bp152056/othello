#include "partie.h"

Partie::Partie()
{
    m_nbtour=0;
    p=Plateau();
    j.push_back(Joueur(0,0));
    j.push_back(Joueur(1,1));
    caqui=0;

}
Partie::~Partie()
{

}

void Partie::setCaqui()
{
    if(caqui==1)
    {
        caqui=0;
    }
    else
    {
        caqui=1;
    }
}

bool Partie::getCaqui()
{
    return(caqui);
}

Plateau Partie::getPlateau()
{
    return(p);
}

Joueur Partie::getJoueur(int num)
{
    return(j[num-1]);
}

void Partie::Menu()
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance(); // Alloue la m�moire du pointeur

    pConsole->gotoLigCol(0,4);
    pConsole->setColor(COLOR_RED);
    std::cout << "2K17 EXTREME OTHELLO SIMULATOR";
    pConsole->setColor(COLOR_DEFAULT);
    pConsole->gotoLigCol(2,8);
    std::cout << "Nouvelle partie";
    pConsole->gotoLigCol(4,8);
    std::cout << "Charger partie";
    pConsole->gotoLigCol(6,8);
    std::cout << "ESC pour quitter";
    pConsole->gotoLigCol(2,8);
    switch(choixMenu())
    {
    case 0:
        commencerPartie();
        break;
    case 1:
        break;
    case 2:
        break;
    }

    Console::deleteInstance(); // Lib�re la m�moire du pointeur !
}

int Partie::choixMenu()
{
    bool quit=false;
    Console* pConsole = NULL;
    pConsole = Console::getInstance(); // Alloue la m�moire du pointeur
    pConsole->gotoLigCol(2,8);
    while (!quit)
    {
        if (pConsole->isKeyboardPressed())
        {
            int key = pConsole->getInputKey();
            //std::cout << key;
            /// fl�che haut : 72, bas : 80, droite : 77, gauche : 75

            if (key == 72) //d�placement curseur haut
            {
                pConsole->moveCurseur(-2,0);
                if(pConsole->getLig()==0)
                {
                    pConsole->gotoLigCol(6,8);
                }
            }

            if (key == 80 ) //d�placement curseur bas
            {
                pConsole->moveCurseur(2,0);
                if(pConsole->getLig()==8)
                {
                    pConsole->gotoLigCol(2,8);
                }
            }

            pConsole->gotoLigCol(pConsole->getLig(),8);

            if (key == 13)
            {
                if(pConsole->getLig()==2)
                {
                    return(0);
                }
                else if(pConsole->getLig()==4)
                {
                    return(1);
                }
                else if(pConsole->getLig()==6)
                {
                    return(2);
                }
                quit=true;
            }

            if (key == 27)
            {
                return(2);
                quit = true;
            }
        }
    }
    Console::deleteInstance(); // Lib�re la m�moire du pointeur !
}

void Partie::commencerPartie()
{
    bool stop=0;
    int tour=1;
    bool finpartie=0;
    bool cpossible[2];
    cpossible[0]=1;
    cpossible[1]=1;
    Console* pConsole = NULL;
    pConsole = Console::getInstance(); // Alloue la m�moire du pointeur
    pConsole->gotoLigCol(0,0);
    p.afficherPlateau();
    Console::deleteInstance(); // Lib�re la m�moire du pointeur !

    while(stop==0)
    {
        if(j[caqui].getia()==0)
        {
            cpossible[caqui]=j[caqui].cestpossibleoupas(&p);
            if(cpossible[caqui])
            {
                j[caqui].poserPion(&p,tour);
            }
            tour++;
            setCaqui();
            p.afficherPions();
        }
        else
        {
            cpossible[caqui]=j[caqui].cestpossibleoupas(&p);
            if(cpossible[caqui])
            {
                j[caqui].vidertabia(&p);
                j[caqui].remplirtabia(&p);
                j[caqui].affichtabia();
                j[caqui].poseria(&p);

            }

            p.afficherPions();
            setCaqui();
            tour++;

        }
        if(cpossible[0]==0 && cpossible[1]==0)
        {
            stop=1;
            kikagagne();
        }

    }

}


void Partie::kikagagne()
{
    int blanc=0;
    int noir=0;
    for (int i=0; i<8; i++)
    {
        for (int j=0; j<8; j++)
        {
            if(p.gettab(j,i)==1)
            {
                noir++;
            }
            if(p.gettab(j,i)==2)
            {
                blanc++;
            }
        }
    }

    if(noir==blanc)
    {
        std::cout<<"merde alors";
    }
    else if(noir>blanc)
    {
        std::cout<<"bg les noirs avec "<<noir<<" pions";
    }
    else if(noir<blanc)
    {
        std::cout<<"encore un blanc qui gagne avec "<<blanc<<" pions";
    }
}
